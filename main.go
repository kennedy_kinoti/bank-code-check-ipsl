package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
)

var router *chi.Mux
var db *sql.DB

const (
	from = "kennedy@ipayafrica.com"
	pass = "Kenya2030"
	to   = "kennedy@ipayafrica.com"
)

// Bank Code struct
type BankCode struct {
	ID        int    `json:"id"`
	BankName  string `json:"bank_name"`
	BankID    string `json:"bank_id"`
	BankLimit string `json:"bank_limit"`
	Status    bool   `json:"status"`
}

func init() {
	router = chi.NewRouter()
	router.Use(middleware.Recoverer)

	var err error

	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading configuration from ENV")
	}

	dbName := os.Getenv("dbName")
	dbPass := os.Getenv("dbPass")
	dbHost := os.Getenv("dbHost")
	dbPort := os.Getenv("dbPort")

	dbSource := fmt.Sprintf("root:%s@tcp(%s:%s)/%s?charset=utf8", dbPass, dbHost, dbPort, dbName)
	db, err = sql.Open("mysql", dbSource)
	catch(err)
}

func routers() *chi.Mux {
	router.Get("/", ping)

	router.Get("/check/{id}", CheckCode)

	return router
}

// server starting point
func ping(w http.ResponseWriter, r *http.Request) {
	respondwithJSON(w, http.StatusOK, map[string]string{"message": "Welcome to iPay Pesalink Bank Code Check"})
}

//-------------- API ENDPOINT ------------------//

// CheckCode check if bank code exists
func CheckCode(w http.ResponseWriter, r *http.Request) {
	payload := BankCode{}
	id := chi.URLParam(r, "id")

	row := db.QueryRow("Select id, bank_name, bank_id, bank_limit, status From bank_codes where bank_id=?", "BANK"+id)

	err := row.Scan(
		&payload.ID,
		&payload.BankName,
		&payload.BankID,
		&payload.BankLimit,
		&payload.Status,
	)

	if err != nil {
		respondwithJSON(w, http.StatusNotFound, map[string]string{"status": "false"})

		return
	}

	respondwithJSON(w, http.StatusOK, payload)

	go LogRequest(payload)
}

// LogRequest function handles payload logging
func LogRequest(b BankCode) {

	bankName := b.BankName
	bankId := b.BankID

	f, err := os.OpenFile("text.log",
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()

	logger := log.New(f, "payload ", log.LstdFlags)
	logger.Println(bankId + ":" + bankName)
}

func main() {
	routers()
	http.ListenAndServe(":9000", Logger())
}

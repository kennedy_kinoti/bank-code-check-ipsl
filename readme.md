# PESALINK Bank Code API

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites
- GO v1.10.4
- `chi` Routing Package
- `chi/middleware` chi middleware Manager
- `go-sql-driver/mysql` Database
- `godotenv` dotenv



- Using the existing .env.example template, configure your environment variables and save the file .env

## Launching the program (Development)
- Run a go run *.go command in your terminal and the program will launch on your specified port in the .env file

## Creating a Service through Systemd (Preferred deployment process)
Assuming the Go executable is placed in the folder /home/{user}/{project_name}/bank-check-api of the Ubuntu VPS, the executable needs to be added to the startup, in case the server is restarted for unknown reasons:

`sudo touch /lib/systemd/system/bank-check-api.service`
Change the permissions of that file:

`sudo chmod -R 755 /lib/systemd/system/bank-check-api.service`
Open the systemd file with your favorite editor

`sudo nano /lib/systemd/system/bank-check-api.service`
Next, inserted the following into the file through nano or any other editor.

```
[Unit]
Description=bank-check-api

[Service]
WorkingDirectory=/home/{user}/{project_name}/
Type=simple
Restart=always
RestartSec=5s
ExecStart=/home/{user}/{project_name}/bank-check-api

[Install]
WantedBy=multi-user.target
```

This allows you to start your binary/service/webapp with:
`sudo service bank-check-api start`

To enable it on boot, type:
`sudo service bank-check-api enable`

Don’t forget to check if everything’s cool through:
`sudo service bank-check-api status`

Example output on the staging server under user kennedy on project folder registration:

````
bank-check-api.service - bank-check-api
   Loaded: loaded (/lib/systemd/system/bank-check-api.service; enabled; vendor preset: enabled)
   Active: active (running) since Fri 2019-01-11 08:03:05 UTC; 35s ago
 Main PID: 15358 (bank-check-api)
    Tasks: 4
   Memory: 1.0M
      CPU: 3ms
   CGroup: /system.slice/bank-check-api.service
           └─15358 /home/kennedy/registration/bank-check-api

Jan 11 08:03:05 dev-01 systemd[1]: bank-check-api.service: Service hold-off time over, scheduling restart.
Jan 11 08:03:05 dev-01 systemd[1]: Stopped bank-check-api.
Jan 11 08:03:05 dev-01 systemd[1]: Started bank-check-api.
Jan 11 08:03:05 dev-01 bank-check-api[15358]: Using port: 8123
Jan 11 08:03:21 dev-01 systemd[1]: Started bank-check-api.
````

## API End Points

#### Check if Bank Code Exists
    Path : /check/{bank_code}
    Method: GET
    Fields: bank_code
    Response: 201

## Running the tests
Onging development and deployment tests

## Deployment
Not yet deployed to a live system

## Built With
- GoLang - The core backend language

## Authors
Kennedy Kinoti

## Acknowledgments
Hat tip to anyone whose code was used

As Henry Ford said:
> If you think you can do a thing or think you can't do a thing, you're right.

